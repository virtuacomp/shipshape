# shipshape
Construct 2 project source for Shipshape

### Game description ###

Grab your friends and set sail for treasure with Shipshape! Up to four players can cooperate in shooting at a colorful gem lodged in the center of an island. Cause that's what pirates do, apparently.

### What is this? ###

[Shipshape](http://virtually-competent.itch.io/shipshape) was originally made for the large touchscreen table housed in the children's section of the Iowa City Public Library and planned to be updated based on the gameplay metrics and feedback from said children. Of course, we got busy with other projects and never returned to Shipshape. As we have no further plans to update Shipshape, the team felt it appropriate to release the source files to the public.

### How do I get set up? ###

The source for Shipshape was created in [Scirra Construct 2](https://www.scirra.com/construct2), so you'll need that program to edit/run the project. You should be able to open it with the free version, but you won't be able to export the game as it makes some use of the paid version's "Families" feature.

### Who do I talk to? ###

As I already mentioned, Shipshape has been abandoned and is no longer in development. However, if you need to hold someone accountable for this mess, you can contact [@donkeyspaceman on Twitter](https://twitter.com/donkeyspaceman).
